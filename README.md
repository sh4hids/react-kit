# ReactJS Starter Kit
This is a very opinionated ReactJS starter kit which includes Styled Components, Material UI, Axios, Redux.

## User Guide
To use this kit please follow the follwing instructions.

- Clone the repository

```bash
$ git clone git@gitlab.com:sh4hids/react-kit.git
```

- Change the remote URL

```bash
$ git remote rm origin
$ git remote add origin your-new-repo-url
$ git config master.remote origin
$ git config master.merge refs/heads/master
```
*Note: You might need to remove the `README.md` if your repository already has one.*

- Install and run the project

```bash
$ npm install
$ npm start
```

- Change project name and necessary files

Edit `package.json, public/index.html, public/manifest.json` accordingly to get started.
