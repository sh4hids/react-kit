import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Text, StyledButton } from './ui/base-kits';
import Button from '@material-ui/core/Button';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Text>
          To get started, edit <code>src/App.js</code> and save to reload.
        </Text>
        <Button>MUI</Button>
        <StyledButton>Styled</StyledButton>
      </div>
    );
  }
}

export default App;
