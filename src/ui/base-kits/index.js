import Text from './Text';
import StyledButton from './StyledButton';

export {
  Text,
  StyledButton,
};
