import styled from 'styled-components';

const Text = styled.p`
  font-size: 1.2rem;
  font-family: sans-serif;
  color: palevioletred;
`;

export default Text;
