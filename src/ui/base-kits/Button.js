import styled from 'styled-components';

const Button = styled.button`
  background: palevioletred;
  color: white;
  text-transform: uppercase;
  padding: 8px 16px;
  font-size: 1.2rem;
  border: none;
  border-radius: 4px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.24),
              0px 4px 4px rgba(0, 0, 0, 0.25);
  outline: none;
  cursor: pointer;
  
  &:hover {
    background: seagreen;
  }
`;

export default Button;
